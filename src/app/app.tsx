import { Switch, Route } from "react-router";

/**
 * The app component must contain the browser router and components that are common to all the application such as a generic snackbar or a spinner.
 * Thoose common components shouldn't be visible by default or they will affect the the layout of every pages.
 * It's still possible to add a common header, drawer or footer here if you're sure you want it on every page.
 */

 import {Page1,Page2}from  "pages"

export function App() {

    return (
        <>
            <Switch>
                <Route path="/page1"  component={Page1}/>
                <Route path="/page2" component={Page2}/>
            </Switch>
            {/* 
            <SnackbarPool />
            <MsgboxPool />
            <AppSpinner /> 
            */}
        </>
    )
}




