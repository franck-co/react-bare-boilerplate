import React from 'react';
import ReactDOM from 'react-dom';

import { App } from 'app/app';

import { Router } from 'react-router-dom'
import { history } from './app/history'
import { ConditionalWrapper } from 'core/utils'

const isStrictMode = true


ReactDOM.render(

  <ConditionalWrapper condition={isStrictMode} wrapper={(children) => <React.StrictMode>{children}</React.StrictMode>}>

    <Router history={history}>
      <App />
    </Router>

  </ConditionalWrapper>

  , document.getElementById('root')
);
