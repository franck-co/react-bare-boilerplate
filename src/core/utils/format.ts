// import moment from '../initLibs/moment'
// export {default as capitalize} from 'capitalize'
export function dayName(dayId){
    return {
      1:'lundi',
      2:'mardi',
      3:'mercredi',
      4:'jeudi',
      5:'vendredi',
      6:'samedi',
      7:'dimanche'
            }[dayId]
  }

  export const percentFormat = (number, decimalPlaces = 0)=>{

    if(typeof number !== "number" || isNaN(number)) return "";
    return (number*100).toFixed(decimalPlaces).toString() + "%"
  }
  
//   /*
//   export const capitalize = (s) => {
//     if (typeof s !== 'string') return ''
//     return s.charAt(0).toUpperCase() + s.slice(1)
//   }*/
  
//   //@ts-ignore
//   String.prototype.capitalize = function(){
//     return this.charAt(0).toUpperCase() + this.slice(1)
//   };
  
  
export function formatString( number,mask) {
    var s = ''+number, r = '';
    for (var im=0, is = 0; im<mask.length && is<s.length; im++) {
      r += mask.charAt(im)=='X' ? s.charAt(is++) : mask.charAt(im);
    }
    return r;
  }    
  
  export function formatBoolean( bool,strTrue='oui',strFalse='non',strNull='inconnu') {
    //alert(bool)
    if(bool===false) return strFalse
    if(bool===true) return strTrue
    if(bool===null || bool === undefined) return strNull
  }    

  //format phone number for display (removes +33 and add spaces for french number only)
  export function formatPhoneNumber(phoneNumber){
    if (!phoneNumber) return ''
    const cleaned = phoneNumber.replace(/ /g,"")
    if(cleaned.substr(0,3) === "+33"){
      return formatString(cleaned.replace("+33","0"),"XX XX XX XX XX")
    }else{
      return phoneNumber
    }
  }



  //convert phone number to international format without spaces
export function standardizePhoneNumber(phoneNumber){
  if (!phoneNumber) return null
  const cleaned = phoneNumber.replace(/ /g,"")
  if(phoneNumber.substr(0,1)==="+")return cleaned;
  else return "+33" + cleaned.substr(1,9)
  }

//   export function dateFormatStandard(dateObj) {
//     return moment(dateObj).format('YYYY-MM-DD HH:mm:ss')
// }

export function customRound(value: number, precision1: string, mode :  "round" | "floor" | "ceil" = "round" ){

  const tofixedValue = precision1.split(".")[1]?.length ;
  const precision = parseFloat(precision1);
  const quotient = Math.floor(value/precision);

  if (mode === 'floor') {

      return (quotient*precision).toFixed(tofixedValue) ;

  } else if (mode === 'ceil') {

      return ((quotient+1)*precision).toFixed(tofixedValue) ;

  } else if (mode === 'round') {

      if ( Math.abs(value - quotient*precision) <= Math.abs(value - (quotient+1)*precision) ) {

          return (quotient*precision).toFixed(tofixedValue) ;
  
      } else {
          return ((quotient+1)*precision).toFixed(tofixedValue) ;
      }
  }
}

export function joinEt(array: string[]):string{
  const lastItem = array.pop()
  const finalArr = []

  array.length && finalArr.push(array.join(", "))
  lastItem && finalArr.push(lastItem)
  
  return finalArr.join(" et ")
}