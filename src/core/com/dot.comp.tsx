import { CSSProperties } from 'react';

const style = (props):CSSProperties=>({
        height:  props.size || '11px',
        width:  props.size || '11px',
        backgroundColor: props.color,
        borderRadius: '50%',
        display: 'inline-block',
        marginInlineStart: props.marginInlineStart || '10px',
        marginInlineEnd:  props.marginInlineEnd || '10px',
        verticalAlign: 'middle',
        textAlign: props.center || 'center'
})


interface dotProps {

    /**css color prop */
    color:string

    /** ex: '11px' */
    size?:string;

    marginInlineStart?:string
    marginInlineEnd?:string

    /**css textAlign prop */
    center?:string
}

export const Dot = (props:dotProps)=>{
    return(
        <span style={style(props)} ></span>
    )
}