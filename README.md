# React + easy-peasy boilerplate
<br>
<br>

## __Configuration__


### __Install dependencies__ ###
```
yarn install
```
<br>

### __Keeping packages up to date__ ###
```
yarn upgrade-interactive --latest
```
Press `space` to select, `a` to toggle all, `i` to inverse selection

<br>


### __Environment variables__

You need then to configure environment variables. These variables are accessible in code using `process.env.MY_ENV_VARIABLE` and are replaced during build (either react-scripts build and react-scripts build).
| Variable name| Example value | Description |
|     :---     |    :---       |     :---    |
| PUBLIC_URL | http://dev.dopple.fr:3100 | Used in `public/index.html` and `public/manifest.json` |
|REACT_APP_DOPPLE_API_URL|http://dev.dopple.fr:4100 OR https://api.dev.dopple.fr|Back-end server location|
|REACT_APP_YOUSIGN_URL|https://staging-app.yousign.com OR https://webapp.yousign.com|target url of yousign website when opening links|
| PORT | 4000 | running port of the development server included in react-scripts start|

<br>
<br>

### __Where to define env variables ?__

| Config |  Description |
|     :---     |    :---      |
|.env.development | specific config for the developper when developing on his own |
|.env.domain | config when running on a shared server. The PORT has to match the nginx config with the dev domain name (app.dev.dopple.fr)<br>Only one developer at a time can run tests using the dev domain name |
|prod.dockerfile<br>staging.dockerfile| For production or staging pipelines, env variables defined in the Dockerfile are added when building image. Example :<br>`ENV PUBLIC_URL=https://app.dopple.fr`<br>`ENV REACT_APP_DOPPLE_API_URL = https://api.dopple.fr`<br>`ENV REACT_APP_YOUSIGN_URL = https://webapp.yousign.com`|


## __Debugging__

Debbuging is configured for vscode in `.vscode`  folder


To debug you need first to launch the dev server and then start the debugger. It will try to open a chrome tab at the url specified in `.vscode/launch.json` and attach to it.

You can lauch the debugger with or without the common domain name.

1. `npm run start:dev`
2. go to debug panel and lauch `debug:dev`



<br>
Note : Chrome extensions like redux devtools need to be installed every time.
<br>
<br>

## __Build__

For staging or production, the project needs a optimized build.
The build is done when building docker image.
The env variables used are those defined in prod.dockerfile or staging.dockerfile

**You do not need to build in development**

<br>

## __Available Scripts__

In the project directory, you can run:

### `yarn start:dev`

Runs the app in the development mode.<br />
The url is defined in .env.development

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

When working with vscode in remote ssh, the port may be auto-forwarded to your local machine and you will be able to access the website at http://localhost:port too.

### `yarn test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn run build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## __Usefull packages__
| name | description |
| --- | --- |
| clsx | A tiny utility for constructing className strings conditionally. |
| capitalize | capitalize names |
| ts-mixer | brings mixins to TypeScript without trouble (kind of multiple class inheritance) |
| jwt-decode ??? in front-end ?| decode json web tokens |
| madge | detect circular dependency |
| dottie | access and set nested objects with 'paths.as.strings' |